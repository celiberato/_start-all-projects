export BASE_PROJETOS="/home/celiberato/projects/clones"

######################### POSTGRESQL ###################
cd $BASE_PROJETOS/starwars-game-postgres-docker-jenkins/devops/postgres
sudo docker-compose down
(sudo docker-compose up&)


###################################################### HTML / NODEJS ###########################################################
#-------- PORTA 9000 ----------
#sudo kill -9 `sudo lsof -t -i:9000`
cd $BASE_PROJETOS/ebac-curso-media-css
yarn install
yarn add webpack webpack-cli --dev
yarn run build
(yarn run dev&)

# -------- PORTA 9002 ----------
sudo kill -9 `sudo lsof -t -i:9002`
cd $BASE_PROJETOS/ebac-curso-sass
sudo yarn install
sudo rm -rf /usr/bin/gulp
sudo npm install -g gulp-cli
(gulp serve&)

# -------- PORTA 9020 ----------
sudo kill -9 `sudo lsof -t -i:9020`
cd $BASE_PROJETOS/ebac-frontend/ebac/curso-arrowfunctions
(http-server ./exercicio-01 -o funcoes.html -p 9020&)

# -------- PORTA 9021 ----------
sudo kill -9 `sudo lsof -t -i:9021`
cd $BASE_PROJETOS/ebac-frontend/ebac/frontend-enginner/prontocloud
(http-server . -o saiba-mais.html -p 9021&)

# -------- PORTA 9022 ----------
sudo kill -9 `sudo lsof -t -i:9022`
cd $BASE_PROJETOS/ebac-frontend/ebac/frontend-enginner/prontocloud
(http-server . -o newsletter.html -p 9022&)


# -------- PORTA 9023 ----------
sudo kill -9 `sudo lsof -t -i:9023`
cd $BASE_PROJETOS/webpack-demo-minimo
npm install
npm run build
(npm run start&)

###################################################### PYTHON ###########################################################


# -------- PORTA 9041 ----------
sudo kill -9 `sudo lsof -t -i:9041`
cd $BASE_PROJETOS/curso-django-bookstore-python/bookstore
###########python3.10 nage.py createsuperuser

########### virtualenv -p python3 bookstore       SOMENTE PRIMEIRA VEZ
source bookstore/bin/activate
pip install -r requirements.txt
poetry run python manage.py migrate

#########  python manage.py createsuperuser

(sudo docker-compose up --build&)



# -------- PORTA 9042 ----------
sudo kill -9 `sudo lsof -t -i:9042`
cd $BASE_PROJETOS/app-django-from-zero/app-from-zero
virtualenv -p python3 app_from_zero
source app_from_zero/bin/activate
pip install -r requirements.txt
(sudo docker-compose up --build&)


###################################################### NEWMAN ###########################################################

# -------- PORTA 3000 ----------
sudo kill -9 `sudo lsof -t -i:3000`
##########cd $BASE_PROJETOS/teste-postman-serverest/ServerRest
##########(sudo npx serverest@latest&)

# -------- PORTA 9043 ----------
sudo kill -9 `sudo lsof -t -i:9043`
cd $BASE_PROJETOS/teste-postman-serverest
yarn install
sudo npm install -g newman newman-reporter-html newman-reporter-htmlextra
sudo newman run postman_collection.json -e environment.json -r htmlextra --reporter-htmlextra-export ./results-newman-report/report-newman.html
(http-server ./results-newman-report -o report-newman.html -p 9043&)


###################################################### CYPRESS API ###########################################################


# -------- PORTA 9044 ----------
sudo kill -9 `sudo lsof -t -i:9044`
cd $BASE_PROJETOS/teste-api-cypress
yarn install
sudo npm install -g --unsafe-perm=true --allow-root
rm -rf mochawesome-report/*.*
npm run cy:run
npm run cy:prepare-report
npm run cy:generate-report&&
(http-server ./report -o report.html -p 9044&)


###################################################### CYPRESS USER INTERFACE ###########################################################

# -------- PORTA 9045 ----------
sudo kill -9 `sudo lsof -t -i:9045`
cd $BASE_PROJETOS/teste-lojaebac-cypress
yarn install
sudo npm install -g --unsafe-perm=true --allow-root
npm run cy:report && (npm run cy:open&)
###### https://dashboard.cypress.io/projects/4zagvu


###################################################### CYPRESS USER INTERFACE ###########################################################
# -------- PORTA 9050 ----------
sudo kill -9 `sudo lsof -t -i:9050`
cd $BASE_PROJETOS/bycoders-desafio-angular
yarn install
(sudo docker-compose up --build&)


# -------- PORTA 9051 ----------
#sudo kill -9 `sudo lsof -t -i:9051`
#cd $BASE_PROJETOS/telerisco-dashboard-faturamento-angular
#npm install
#npm run build && (npm run start&)


###################################################### SPRING BOOT ###########################################################


# -------- PORTA 9060 ----------
sudo kill -9 `sudo lsof -t -i:9063`
cd $BASE_PROJETOS/bycoders-desafio-backend
mvn clean package
cd $BASE_PROJETOS/bycoders-desafio-backend/target
(java -jar desafio.bycoders-backend-0.0.1-SNAPSHOT.jar&)


#----------------- PORTA 907O-------------------
cd $BASE_PROJETOS/starwars-game-auth-keycloak/docker
#########sudo docker-compose down
#########(sudo docker-compose -f docker-compose-external.ymnl up --build&)

# -------- PORTA 9063 ----------
sudo kill -9 `sudo lsof -t -i:9063`
cd $BASE_PROJETOS/starwars-game-auth-keycloak
mvn clean package
cd $BASE_PROJETOS/starwars-game-auth-keycloak/target
(java -jar starwars-game-auth-keycloak-0.0.1-SNAPSHOT.jar&)

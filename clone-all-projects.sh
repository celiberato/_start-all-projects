export BASE_PROJETOS=/home/celiberato/projects/clones

mkdir $BASE_PROJETOS
cd $BASE_PROJETOS

git clone https://gitlab.com/celiberato/starwars-game-postgres-docker-jenkins.git

git clone https://gitlab.com/celiberato/ebac-curso-media-css.git
git clone https://gitlab.com/celiberato/ebac-curso-sass.git
git clone https://gitlab.com/celiberato/ebac-curso-gulp.git
git clone https://gitlab.com/celiberato/ebac-frontend.git
git clone https://gitlab.com/celiberato/data-manipulation-angular.git
git clone https://gitlab.com/celiberato/ajax-project.git
git clone https://gitlab.com/celiberato/webpack-demo-minimo.git
git clone https://gitlab.com/celiberato/projeto-css.git
git clone https://gitlab.com/celiberato/curso-django-bookstore-python.git
git clone https://gitlab.com/celiberato/django_blank
git clone https://gitlab.com/celiberato/teste-postman-serverest.git
git clone https://gitlab.com/celiberato/teste-api-cypress.git
git clone https://gitlab.com/celiberato/teste-lojaebac-cypress.git
git clone https://gitlab.com/celiberato/bycoders-desafio-angular.git
git clone https://gitlab.com/celiberato/telerisco-dashboard-faturamento-angular.git
git clone https://gitlab.com/celiberato/bycoders-desafio-backend.git

git clone https://gitlab.com/celiberato/starwars-game-auth-keycloak.git

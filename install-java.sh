sudo apt-get install git
sudo apt-get install curl
sudo apt-get install vi


# install java 11
sudo apt update &&
sudo apt install openjdk-11-jdk &&
java -version


# install docker
sudo apt-get update &&
#######sudo apt-get remove docker docker-engine docker.io &&
sudo apt install docker.io &&
sudo systemctl start docker &&
sudo systemctl enable docker &&
docker --version


# install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version


sudo apt install maven





export BASE_PROJETOS="/home/celiberato/projects/clones"

./execute-postgres.sh

./execute-media-css.sh
./execute-sass.sh
./execute-arrowfunctions.sh
./execute-newman.sh
./execute-landing-page.sh
./execute-newsletter.sh
./execute-saiba-mais.sh
./execute-data-manipulation-angular.sh
./execute-ajax-project.sh
./execute-webpack-demo-minimo.sh
./execute-projeto-css.sh
./execute-python-blank.sh
./execute-python-bookstore.sh
./execute-teste-api-cypress.sh
./execute-teste-lojaebac-cypress.sh
./execute-desafio-backend.sh
./execute-desafio-angular.sh

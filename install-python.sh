sudo apt update && sudo apt upgrade -y
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:deadsnakes/ppa -y


sudo apt install python3.10

sudo apt install python3-pip


alias pip=pip3
alias python=python3.8


pip install pytest-django
pip install Faker
pip install factory_boy

sudo apt install python3-django
sudo apt-get install python3-django-extensions

sudo apt-get install python-djangorestframework

sudo apt install virtualenv

sudo curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3.10 -


source $HOME/.poetry/env
